package com.rognlien.even.mobile_labs_assignment_1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class StartActivity extends FragmentActivity {

    private GoogleMap map;
    private Location myLocation;
    private Marker meMarker;
    private LocationManager locManager;

    private static final String PREV_LATITUDE = "latitude";
    private static final String PREV_LONGITUDE = "longitude";
    private static final String PREV_ALTITUDE = "altitude";
    private static final String PREV_DATETIME = "datetime";
    private static final String PREV_ZOOM = "zoom";
    private static final String MY_LOCATION = "myLocation";
    private static final String CAMERA_POSITION = "camPosition";

    static final String LOCATION = "location";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        map = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
        locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        loadPreviousSession();
    }


    // This saves the current marker position and camera position.
    // Saved when the screen is flipped, then restored by onRestoreInstanceState()
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if(meMarker != null)
            savedInstanceState.putParcelable(MY_LOCATION, myLocation);
        savedInstanceState.putParcelable(CAMERA_POSITION, map.getCameraPosition());
        super.onSaveInstanceState(savedInstanceState);
    }

    // Called when the the activity is destroyed and recreated,
    // for example after the screen is rotated:
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null) {
            try {
                // Load the location and map position that was before the screen was flipped.
                // The myLocation can still be used as before to get detailed info (coordinates, temperature, etc)
                myLocation = savedInstanceState.getParcelable(MY_LOCATION);
                CameraPosition camPosition = savedInstanceState.getParcelable(CAMERA_POSITION);

                if (myLocation != null) {
                    findViewById(R.id.detailButton).setEnabled(true);
                    meMarker.setPosition(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                }
                if (camPosition != null) {
                    map.moveCamera(CameraUpdateFactory.newCameraPosition(camPosition));
                }
            }
            catch(Exception ex) {
                Toast.makeText(this, "Error: "+ ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }


    // This saves the current session to the preference files when the activity is paused.
    // Called every time focus is going away from the activity. For example when it's closed:
    @Override
    public void onPause() {
        super.onPause();
        if(myLocation != null) {
            SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(PREV_LATITUDE, Double.toString(myLocation.getLatitude()));
            editor.putString(PREV_LONGITUDE, Double.toString(myLocation.getLongitude()));
            editor.putString(PREV_ALTITUDE, Double.toString(myLocation.getAltitude()));
            editor.putFloat(PREV_ZOOM, map.getCameraPosition().zoom);
            editor.putLong(PREV_DATETIME, myLocation.getTime());
            editor.commit();
        }
    }


    // This is called in onCreate after screen is created. It loads the previous
    // position, updates the myLocation and moves the marker to where it was.
    // This is used for recovering the previous session if the app was closed
    private  void loadPreviousSession() {
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        if(sharedPref != null) {
            Location loc = new Location("restore");
            loc.setLatitude(Double.parseDouble(sharedPref.getString(PREV_LATITUDE, "0.0")));
            loc.setLongitude(Double.parseDouble(sharedPref.getString(PREV_LONGITUDE, "0.0")));
            loc.setAltitude(Double.parseDouble(sharedPref.getString(PREV_ALTITUDE, "0.0")));
            loc.setTime(sharedPref.getLong(PREV_DATETIME, 0));
            float zoom = sharedPref.getFloat(PREV_ZOOM, 12);

            if(loc.getLongitude() != 0.0) {
                myLocation = loc;
                moveMapMarker(new LatLng(loc.getLatitude(), loc.getLongitude()), zoom, false);
                findViewById(R.id.detailButton).setEnabled(true);
            }
        }
    }


    // Called when "Find me" is clicked:
    public void findLocation(View v) {
        if ( !locManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            // If GPS is not enabled, show dialog and abort
            (new AlertDialog.Builder(this)
                    .setTitle("GPS disabled")
                    .setMessage("Please enable your GPS")
                    .setNeutralButton("OK", null)).show();
            return;
        }

        // Create dialog to be displayed while GPS is searching:
        final AlertDialog infoDialog = new AlertDialog.Builder(this)
                .setTitle("Please wait...")
                .setMessage("Getting location...")
                .create();
        infoDialog.show();

        // Start getting location updates with 1 sec (1000ms) or 1 m separation
        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                // When new location is received, hide the dialog and set the marker location.
                LatLng myLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                infoDialog.hide();
                myLocation = location;

                // Move marker to the new position
                moveMapMarker(myLatLng, map.getCameraPosition().zoom, true);

                // Enables the "Show details" button when a location is loaded
                findViewById(R.id.detailButton).setEnabled(true);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });
    }

    // Moves the map marker to the given LatLng, possibly with animation
    private void moveMapMarker(LatLng latLng, float zoom, boolean animate) {
        if(meMarker == null) {
            // If marker not created before, create it and add it to map
            meMarker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(latLng.latitude, latLng.longitude))
                    .title("You are here"));
        }
        meMarker.setPosition(latLng);

        // Map animation code found here:
        // http://stackoverflow.com/questions/14074129/google-maps-v2-set-both-my-location-and-zoom-in
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)     // Sets the center of the map to my coordinates
                .zoom(zoom)         // Sets the zoom
                .build();           // Creates a CameraPosition from the builder

        if (animate)
            map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        else
            map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }


    // Starts the information activity:
    public void showInfoActivity(View v) {
        Intent intent = new Intent(StartActivity.this, InfoActivity.class);
        intent.putExtra(LOCATION, myLocation);
        startActivity(intent);
    }


}
