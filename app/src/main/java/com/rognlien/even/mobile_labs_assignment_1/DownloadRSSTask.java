package com.rognlien.even.mobile_labs_assignment_1;

import android.os.AsyncTask;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * This class is a modified version of the RSS-file download example published
 * by the lecturers of the Mobile Development course at HiG:
 * https://bitbucket.org/gtl-hig/imt3662_file_download
 */
public class DownloadRSSTask extends AsyncTask<URL, Void, String> {


    @Override
    protected String doInBackground(URL... urls) {
        return downloadRSS(urls[0]);
    }


    private String downloadRSS(final URL url) {
        InputStream in = null;
        String temperatureCelsius = "not found";

        try {
            in = openHttpConnection(url);
            Document doc = null;
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;

            try {
                db = dbf.newDocumentBuilder();
                doc = db.parse(in);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            }

            doc.getDocumentElement().normalize();
            Node forecastNode = doc.getElementsByTagName("forecast").item(0);
            Node tabularNode = ((Element)forecastNode).getElementsByTagName("tabular").item(0);
            Node hourNode = ((Element)tabularNode).getElementsByTagName("time").item(0);        // Get the first hour (closest to now)
            Node tempNode = ((Element)hourNode).getElementsByTagName("temperature").item(0);
            temperatureCelsius = ((Element)tempNode).getAttribute("value");

        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }

        return temperatureCelsius;
    }

    // opens http connection to the URL
    private InputStream openHttpConnection(final URL url) throws IOException {
        InputStream in = null;
        int response = -1;
        final URLConnection conn = url.openConnection();
        if(!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }
        try {
            final HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();
            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return in;
    }

}
