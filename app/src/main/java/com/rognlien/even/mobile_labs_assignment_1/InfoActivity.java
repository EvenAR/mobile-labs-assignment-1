package com.rognlien.even.mobile_labs_assignment_1;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutionException;



public class InfoActivity extends Activity {
    public static final String LOG_TABLE_NAME = "posAndTempLog";

    // The column names:
    static final String TIME = "time";
    static final String LATITUDE = "latitude";
    static final String LONGITUDE = "longitude";
    static final String ALTITUDE = "altitude";
    static final String CITY = "city";
    static final String TEMPERATURE = "temperature";

    // SQL statement to create table
    public static final String LOG_CREATE_TABLE =
            "CREATE TABLE " + LOG_TABLE_NAME + " ("
            + TIME + " VARCHAR PRIMARY KEY,"
            + LATITUDE + " DOUBLE,"
            + LONGITUDE + " DOUBLE,"
            + ALTITUDE + " INTEGER,"
            + CITY + " VARCHAR,"
            + TEMPERATURE + " VARCHAR"
            + ");";

    private DatabaseHelper dbHelper;
    private DownloadRSSTask rssDownloader;
    private Location myLocation;
    private Address address;
    private String temperature;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        rssDownloader = new DownloadRSSTask();
        dbHelper = new DatabaseHelper(InfoActivity.this);
        Bundle extras = getIntent().getExtras();

        if(extras != null) {
            myLocation = extras.getParcelable(StartActivity.LOCATION);
            displayInformation(myLocation);
        }
    }

    private void displayInformation(Location loc) {
        if(loc != null) {
            Geocoder geocoder = new Geocoder(InfoActivity.this, Locale.ENGLISH);
            try {
                address = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1).get(0);
            } catch (IOException e) {
                Toast.makeText(InfoActivity.this, "Could not find address: " + e.getMessage(), Toast.LENGTH_LONG).show();
            }

            // Download the temperature using XML from www.yr.no
            // The GUI thread will stop responding while its waiting for rssDownloader to return the temperature
            try {
                temperature = rssDownloader.execute(new URL("http://www.yr.no/sted/Norge/postnummer/" + address.getPostalCode() + "/varsel_time_for_time.xml")).get();
            } catch (InterruptedException e) {
                Toast.makeText(InfoActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            } catch (ExecutionException e) {
                Toast.makeText(InfoActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            } catch (MalformedURLException e) {
                Toast.makeText(InfoActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            }

            // Put all the information into a string and display it
            TextView textView = (TextView)findViewById(R.id.posInfo);
            String info = "You are currently in " + address.getLocality() + " in " + address.getCountryName()
                    + "\n\nThe local temperature is " + temperature + "°C"
                    + "\n\nYour coordinates are:"
                    + "\n   Latitude:   " + loc.getLatitude()
                    + "\n   Longitude:  " + loc.getLongitude()
                    + "\n   Altitude:   " + loc.getAltitude();
            textView.setText(info);

            reLoadList();
        }

    }

    // This function runs when "Save to log" is pressed
    // The displayed data is stored into the SQLite database
    public void saveToLog(View v)  {
        try {
            SQLiteDatabase db = dbHelper.getWritableDatabase();

            // Get timestamp
            // (http://stackoverflow.com/questions/12747549/android-location-time-into-date)
            DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date(myLocation.getTime());
            String formattedDate = format.format(date);

            // Collect values
            ContentValues values = new ContentValues();
            values.put(TIME, formattedDate);
            values.put(LATITUDE, myLocation.getLatitude());
            values.put(LONGITUDE, myLocation.getLongitude());
            values.put(ALTITUDE, (int)myLocation.getAltitude());
            values.put(CITY, address.getLocality());
            values.put(TEMPERATURE, temperature);

            // Insert them in the database table
            db.insertOrThrow(LOG_TABLE_NAME, null, values);
            db.close();

            reLoadList();
            Toast.makeText(this, "Saved!", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e) {
            if(e.getMessage().contains("code 19"))  // If the date (primary key) is already in the db table
                Toast.makeText(this, "Already saved!", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Error when saving: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    // This is used to refresh the data in the history list. It fetches all data from the
    // SQLite database and displays it in the list with two-line list entries.
    public void reLoadList() {
        try {
            ListView listView = (ListView)findViewById(R.id.historyList);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            // Get cursor to the columns from the log table
            Cursor c = db.query(LOG_TABLE_NAME, null, null, null, null, null, null);

            // Start at the last row (I want the newest object on top of the list)
            c.moveToLast();

            // Some of this code is from here:
            // http://stackoverflow.com/questions/21335623/android-get-listview-value-from-an-2-line-listview-on-onclick
            ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
            while (!c.isBeforeFirst()) {
                HashMap<String,String> item = new HashMap<String,String>();

                // Create first line
                String s1 = c.getString(c.getColumnIndex(CITY)) + " "
                        + c.getString(c.getColumnIndex(TIME));

                // Create second line
                String s2 = c.getString(c.getColumnIndex(TEMPERATURE)) + "\u00b0C"
                        + " (" + c.getString(c.getColumnIndex(ALTITUDE)) + "m - "
                        + c.getString(c.getColumnIndex(LATITUDE)) + ", "
                        + c.getString(c.getColumnIndex(LONGITUDE)) + ")";

                item.put("line1", s1);
                item.put("line2", s2);
                list.add(item);

                // Move backwards in the rows
                c.moveToPrevious();
            }


            SimpleAdapter sa = new SimpleAdapter(this, list, android.R.layout.two_line_list_item ,new String[] { "line1","line2" }, new int[] {android.R.id.text1, android.R.id.text2});
            listView.setAdapter(sa);

            c.close();
            db.close();
        }
        catch(Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
